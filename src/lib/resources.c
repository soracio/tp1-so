#include "resources.h"
#include <stdlib.h>
#include <string.h>

static char *pid_to_string_rec(pid_t pid, size_t depth, size_t* size);

char *pid_to_string(pid_t pid, size_t* size){
  if (pid == 0)
  {
    char *string = malloc(2);
    strcpy(string, PID_0);
    return string;
  }
  return pid_to_string_rec(pid, 2, size);
}

static char *pid_to_string_rec(pid_t pid, size_t depth, size_t* size){
  (*size)++;
  char *string;
  if(pid == 0){
    string = malloc(*size);
    string[(*size)-1] = 0;
    return string;
  }
  string = pid_to_string_rec(pid/10, depth+1,size);
  string[(*size)-depth] = (pid%10) + '0';
  return string;
}